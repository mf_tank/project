<?php

use yii\db\Migration;

/**
 * Class m190607_081728_tbl_change_social
 */
class m190607_081728_tbl_change_social extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%social}}', 'sort', $this->integer()->notNull()->defaultValue(500));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%social}}', 'sort');
    }


}
