<?php

use yii\db\Migration;

/**
 * Class m190604_112901_promo_our_photographers
 */
class m190604_112901_promo_our_photographers extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('promo_our_photographers', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'image_id' => $this->integer(),
            'trailer_id' => $this->integer(),
            'title' => $this->string(),
            'description' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ]);

        $this->addForeignKey('photographer_user',
            '{{%promo_our_photographers}}', 'user_id',
            '{{%user}}', 'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('promo_our_photographers');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190604_112901_promo_our_photographers cannot be reverted.\n";

        return false;
    }
    */
}
