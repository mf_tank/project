<?php


use yii\db\Migration;
use app\models\User;

/**
 * Class m150331_072245_tbl_user
 */
class m150331_072245_tbl_user extends Migration
{

    /**
     * Накат миграции
     * @return void
     */
    public function up()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
            'password' => $this->string()->notNull(),
            'auth_key' => $this->string()->notNull(),
            'access_token' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $auth = Yii::$app->authManager;
        $guest = $auth->createRole(User::ROLE_GUEST);
        $auth->add($guest);

        $admin = $auth->createRole(User::ROLE_ADMIN);
        $auth->add($admin);

        $auth->addChild($admin, $guest);
        $auth->assign($admin, 1);


        $user = new User();
        $user->username = 'skugarev.andrey@gmail.com';
        $user->password = '111';
        $user->save();
    }

    /**
     * Откат миграции
     * @return void
     */
    public function down()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAllRoles();
        $auth->removeAll();
        $this->dropTable('{{%user}}');
    }
}
