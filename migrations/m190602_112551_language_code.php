<?php

use yii\db\Migration;

/**
 * Class m190602_112551_language_code
 */
class m190602_112551_language_code extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('lang', 'code', $this->string()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('lang', 'code');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190602_112551_language_code cannot be reverted.\n";

        return false;
    }
    */
}
