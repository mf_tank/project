<div id="center">
    <div id="faq">
        <div class="container">
            <h1>FAQ</h1>

            <div class="faqBlock center-block text-align-left">
                <div class="faq__left">
                    <div class="faq__item">
                        <?php foreach ($faqs as $model) {?>
                            <div class="faq__list">
                                <a href="<?php echo \yii\helpers\Url::to(['faq/view', 'id' => $model->id])?>">
                                    <?php echo $model->name;?>
                                </a>
                            </div>
                        <?php }?>
                    </div>
                </div>
                <div class="faq__right">
                    <div class="faq__item">
                        <div class="faq__listTitle">
                            <?php echo $faq->name;?>
                        </div>
                        <div class="faq__listText">
                            <?php echo $faq->text;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php echo $this->render('/blocks/_discount');?>

    <?php echo $this->render('/blocks/_socials', [
        'socials' => $socials
    ]);?>

</div>
