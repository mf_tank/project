<div id="footer">
    <div class="container">
        <div class="footerBlock center-block text-align-left">
            <div class="footerBlock__left">
                <div class="footerBlock__text">
                    Splashfolio
                    <span>
                        <?php echo \Yii::t('site', 'Photography Tutorials From The Best In The Business');?>
                    </span>
                </div>

                <div class="featured"><?php echo \Yii::t('site', 'Secured with SSL');?></div>
            </div>
            <div class="footerBlock__right">
                <div class="footerBlock__menu">
                    <ul>
                        <?php foreach ($this->context->menuFooter1 as $model) {?>
                            <li>
                                <a href="<?php echo $model->link;?>">
                                    <?php echo $model->name;?>
                                </a>
                            </li>
                        <?php }?>
                    </ul>
                </div>

                <div class="footerBlock__menu">
                    <ul>
                        <?php foreach ($this->context->menuFooter2 as $model) {?>
                            <li>
                                <a href="<?php echo $model->link;?>">
                                    <?php echo $model->name;?>
                                </a>
                            </li>
                        <?php }?>
                    </ul>
                </div>

                <div class="footerBlock__menu">
                    <ul>
                        <?php foreach ($this->context->menuFooter3 as $model) {?>
                            <li>
                                <a href="<?php echo $model->link;?>">
                                    <?php echo $model->name;?>
                                </a>
                            </li>
                        <?php }?>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="footerCopyright">
        <div class="container">
            <div class="footerCopyright__block center-block text-align-left">
                <div class="footerCopyright__left">© Splashfolio, 2018</div>
                <div class="footerCopyright__right">
                    <span>Privacy Policy</span>
                    <span>Terms of Use</span>
                </div>
            </div>
        </div>
    </div>
</div>