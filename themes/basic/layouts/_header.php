<div id="top_menu">
    <div class="container">
        <div class="headerStr">
            <div class="headerStr__logo" style="background: url('/img/logo.png') no-repeat center / contain"></div>

            <div class="top_menu_block">
                <div class="top_menu_block__burger"></div>

                <div class="top_menu_block__menu">
                    <div class="headerStr__menu">
                        <ul>
                            <?php foreach ($this->context->menuHeader as $model) {?>
                            <li>
                                <a href="<?php echo $model->link;?>">
                                    <?php echo Yii::t('site', $model->name);?>
                                </a>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="headerStr__profile">
                        <div class="headerStrLang__anchor">Aleksandr</div>

                        <ul>
                            <li><a href="#"><?php echo \Yii::t('site', 'Profile');?></a></li>
                            <li><a href="#"><?php echo \Yii::t('site', 'Logout');?></a></li>
                        </ul>
                    </div>
                    <div class="headerStr__lang">
                        <div class="headerStrLang__anchor">
                            <?php echo $this->context->language;?>
                        </div>

                        <?php if ($this->context->languages): ?>
                            <ul>
                                <?php foreach ($this->context->languages as $lang): ?>
                                    <li>
                                        <a href="/site/lang/<?=$lang->id?>">
                                            <?=$lang->name?>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="top_menu_block__buy">
                    <div class="top_menu_block__text"><?php echo \Yii::t('site', 'All videos in one bundle. More than 70% discount.');?></div>

                    <div class="discountVideo__price">
                        <div class="splashfolioButton__in"><div class="splashfolioButton button-green"><?php echo \Yii::t('app', 'Purchase');?></div></div>
                        <div class="priceBlock">
                            <span>$1200</span>
                            <span>$3200</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>