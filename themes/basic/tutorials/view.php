<div id="header">
    <div class="headerBlock">
        <div id="headerTutorials">
            <div class="headerBlock__item">
                <div class="headerBlock__img" style="background: url('/img/bg-header.jpg') no-repeat center / cover"></div>

                <!-- <div class="headerPlay" style="background: url('img/tutorials/play-button.svg') no-repeat center / cover"></div>-->


                <div class="container">
                    <div class="headerTutorials_head">

                        <div class="headerTutorials__avatar" style="background: url('/img/ourPhotographers/userpic-2.png') no-repeat center / cover">
                        </div>
                        <h4>Chris Knight</h4>
                    </div>

                    <h2>Creative Photoshop Techniques</h2>

                    <div class="headerBlock__desc">For composite photographers, creating images is bound only by imagination. Dream like photographs with incredible realism can be built from a series of different, and often unrelated, components. </div>

                    <div class="discountVideo__price">
                        <div class="splashfolioButton__in">
                            <div class="splashfolioButton button-orange play_trailer"><div class="display-none">Play </div>Trailer</div>
                            <div class="splashfolioButton button-green">Purchase</div>
                        </div>
                        <div class="priceBlock">
                            <span>$1200</span>
                            <span>$3200</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="headerPlay-popup" class="popup">
    <div class="container">
        <div class="headerBlock__item">
            <div class="headerBlock__img" style="background: url('img/bg-header.jpg') no-repeat center / cover"></div>

            <div class="headerPlay" style="background: url('img/tutorials/play-button.svg') no-repeat center / cover"></div>
        </div>

        <div class="top_menu_block__buy center-block">
            <div class="top_menu_block__text">Landscape Photography in Depth Tutorial</div>

            <div class="discountVideo__price">
                <div class="splashfolioButton__in">
                    <div class="splashfolioButton button-green">Purchase</div>
                </div>
                <div class="priceBlock">
                    <span>$1200</span>
                    <span>$3200</span>
                </div>
            </div>
        </div>
    </div>

    <div id="headerPlay__slider">
        <ul>
            <li>
                <div class="headerPlay__item" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                    <div class="headerPlay" style="background: url('img/tutorials/play-button.svg') no-repeat center / cover"></div>
                </div>
            </li>
            <li>
                <div class="headerPlay__item" style="background: url('img/ourPhotographers/DSC_3448.jpg') no-repeat center / cover">
                    <div class="headerPlay" style="background: url('img/tutorials/play-button.svg') no-repeat center / cover"></div>
                </div>
            </li>
            <li>
                <div class="headerPlay__item" style="background: url('img/ourPhotographers/DSC_2436.jpg') no-repeat center / cover">
                    <div class="headerPlay" style="background: url('img/tutorials/play-button.svg') no-repeat center / cover"></div>
                </div>
            </li>
            <li>
                <div class="headerPlay__item" style="background: url('img/ourPhotographers/DSC_4216.jpg') no-repeat center / cover">
                    <div class="headerPlay" style="background: url('img/tutorials/play-button.svg') no-repeat center / cover"></div>
                </div>
            </li>
            <li>
                <div class="headerPlay__item" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                    <div class="headerPlay" style="background: url('img/tutorials/play-button.svg') no-repeat center / cover"></div>
                </div>
            </li>
            <li>
                <div class="headerPlay__item" style="background: url('img/ourPhotographers/DSC_9860.jpg') no-repeat center / cover">
                    <div class="headerPlay" style="background: url('img/tutorials/play-button.svg') no-repeat center / cover"></div>
                </div>
            </li>
            <li>
                <div class="headerPlay__item" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                    <div class="headerPlay" style="background: url('img/tutorials/play-button.svg') no-repeat center / cover"></div>
                </div>
            </li>
            <li>
                <div class="headerPlay__item" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                    <div class="headerPlay" style="background: url('img/tutorials/play-button.svg') no-repeat center / cover"></div>
                </div>
            </li>
        </ul>

        <div id="headerPlay__arrow" class="display-none">
            <div class="prev"></div>
            <div class="next"></div>
        </div>
    </div>

</div>
<div id="featuredProducts-popup" class="popup">
    <h2>Chris Knight’s Tutorials</h2>

    <div class="featuredProducts__search">
        <input name="search" type="search" placeholder="Search tutorials">
    </div>

    <div class="featuredProducts__item center-block">
        <div class="photoItem" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
            <div class="photoItemBlock">
                <div class="featured">featured</div>
                <h4>Creative Photoshop Techniques</h4>
                <div class="arrow"></div>
            </div>
        </div>

        <div class="photoItem" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
            <div class="photoItemBlock">
                <div class="featured">featured</div>
                <h4>Creative Photoshop Techniques</h4>
                <div class="arrow"></div>
            </div>
        </div>

        <div class="photoItem" style="background: url('img/ourPhotographers/DSC_9860.jpg') no-repeat center / cover">
            <div class="photoItemBlock">
                <div class="featured">featured</div>
                <h4>Creative Photoshop Techniques</h4>
                <div class="arrow"></div>
            </div>
        </div>

        <div class="photoItem" style="background: url('img/ourPhotographers/DSC_2436.jpg') no-repeat center / cover">
            <div class="photoItemBlock">
                <div class="featured">featured</div>
                <h4>Creative Photoshop Techniques</h4>
                <div class="arrow"></div>
            </div>
        </div>

        <div class="photoItem" style="background: url('img/ourPhotographers/DSC_0871_1-Pano.jpg') no-repeat center / cover">
            <div class="photoItemBlock">
                <div class="featured">featured</div>
                <h4>Creative Photoshop Techniques</h4>
                <div class="arrow"></div>
            </div>
        </div>

        <div class="photoItem" style="background: url('img/ourPhotographers/DSC_3132_F.jpg') no-repeat center / cover">
            <div class="photoItemBlock">
                <div class="featured">featured</div>
                <h4>Creative Photoshop Techniques</h4>
                <div class="arrow"></div>
            </div>
        </div>

        <div class="photoItem" style="background: url('img/ourPhotographers/DSC_3132_F.jpg') no-repeat center / cover">
            <div class="photoItemBlock">
                <div class="featured">featured</div>
                <h4>Creative Photoshop Techniques</h4>
                <div class="arrow"></div>
            </div>
        </div>

        <div class="photoItem" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
            <div class="photoItemBlock">
                <div class="featured">featured</div>
                <h4>Creative Photoshop Techniques</h4>
                <div class="arrow"></div>
            </div>
        </div>

        <div class="photoItem" style="background: url('img/ourPhotographers/DSC_4216.jpg') no-repeat center / cover">
            <div class="photoItemBlock">
                <div class="featured">featured</div>
                <h4>Creative Photoshop Techniques</h4>
                <div class="arrow"></div>
            </div>
        </div>

        <div class="photoItem" style="background: url('img/ourPhotographers/DSC_7893.jpg') no-repeat center / cover">
            <div class="photoItemBlock">
                <div class="featured">featured</div>
                <h4>Creative Photoshop Techniques</h4>
                <div class="arrow"></div>
            </div>
        </div>

        <div class="photoItem" style="background: url('img/ourPhotographers/DSC_1349_Panorama2_16bit_100_67.jpg') no-repeat center / cover">
            <div class="photoItemBlock">
                <div class="featured">featured</div>
                <h4>Creative Photoshop Techniques</h4>
                <div class="arrow"></div>
            </div>
        </div>

        <div class="photoItem" style="background: url('img/ourPhotographers/DSC_1349_Panorama2_16bit_100_67.jpg') no-repeat center / cover">
            <div class="photoItemBlock">
                <div class="featured">featured</div>
                <h4>Creative Photoshop Techniques</h4>
                <div class="arrow"></div>
            </div>
        </div>
    </div>
</div>
<div id="center">
    <div id="inside">
        <div class="container">
            <div class="h2"><h2>What’s Inside</h2></div>

            <!-- <div class="discountVideo__guarantee center-block">
                <div class="discountVideo__item">
                    <h4>Downloadable HD Content</h4>

                    <h2>4h 24m / <span>6.4 Gb</span></h2>
                </div>

                <div class="discountVideo__item">
                    <h4>Used software</h4>

                    <div class="software">
                        <div class="software__item" style="background: url('img/tutorials/ps.png') no-repeat center / cover"></div>
                        <div class="software__item" style="background: url('img/tutorials/lr.png') no-repeat center / cover"></div>
                        <div class="software__item" style="background: url('img/tutorials/aurora.png') no-repeat center / cover"></div>
                    </div>
                </div>

                <div class="discountVideo__item">
                    <h4>Available languages</h4>

                    <div class="languages">
                        <span>English</span>
                        <span>German</span>
                        <span>French</span>
                        <span>Russian</span>
                        <span>Japanese</span>
                        <span>Chinese</span>
                        <span>Korean</span>
                    </div>
                </div>
            </div>-->

            <div class="insideList">
                <div class="insideList_item">
                    35 Videos
                </div>

                <div class="insideList_item">
                    4 hours
                </div>

                <div class="insideList_item">
                    Photoshoot Included
                </div>

                <div class="insideList_item">
                    Retouching Included
                </div>

                <div class="insideList_item">
                    Raw Files Included
                </div>

                <div class="insideList_item">
                    Actions Included
                </div>

                <div class="insideList_item">
                    High Quality
                    <span>
                    HD Quality, 4K quality, HD+4Kquality
                </span>
                </div>

                <div class="insideList_item">
                    Subtitles
                    <span>
                    6 languages English, Russian, Spanish, German, Chinese, Japanese
                </span>
                </div>

                <div class="insideList_item">
                    Business Strategies
                </div>

                <div class="insideList_item">
                    Social Media Strategy
                </div>
            </div>

            <div class="softwareBlock">
                <h4>Software Used</h4>

                <div class="softwareList">
                    <div class="softwareList_item">
                        <span>Adobe Photoshop</span>
                        <span>Adobe Lightroom</span>
                        <span>Ptgui</span>
                    </div>

                    <div class="softwareList_item">
                        <span>Adobe Premier CC</span>
                        <span>Helico Focus</span>
                        <span>LRtimelapse</span>
                    </div>

                    <div class="softwareList_item">
                        <span>Adobe After Effects</span>
                        <span>Adobe Lightroom</span>
                    </div>
                </div>
            </div>

            <div class="tripPlanning">
                <div class="tripPlanning_item">
                    <div class="tripPlanning_head">
                        <h4>Composition, trip planning, gear and more.</h4>
                    </div>

                    <div class="tripPlanning_block_in">
                        <div class="tripPlanning_block">
                            <div class="tripPlanning__left">
                                <ul>
                                    <li>Deep dive into landscape photography with numerous samples and tips.</li>
                                    <li>All about filters: polarize, neutral density, gradual density, circular and more.</li>
                                    <li>The art of seeing. Finding locations that give a unique perspective of a known landmark</li>
                                    <li>Planning your trip. Knowing what you will be shooting long before you arrive to the location.</li>
                                    <li>Gear overview. How to not overload yourself with equipment.</li>
                                    <li>Understanding Light (Golden Hour, Blue Hour, Sunrise and Sunset)</li>
                                    <li>Bracketing and multiple exposure blending</li>
                                    <!-- <li>Shooting in challenging weather or high contrast conditions (rain, ice, snow, water).</li>
                                     <li>Dealing with Sun Flare and shooting against the sun</li>
                                     <li>Building Panoramic images and learning about nodal points for impeccable panoramas. Vertical panoramas vs horizontal.</li>
                                     <li>The correct approach to long exposure. Blurring water and skies with long exposures</li>
                                     <li>Introduction to astrophotography.</li>
                                     <li>Master exposure and Histograms.</li>
                                     <li>Shooting in challenging weather or high contrast conditions (rain, ice, snow, water).</li>
                                     <li>Dealing with Sun Flare and shooting against the sun</li>-->
                                </ul>

                                <!--<ul class="more_block">
                                    <li>Planning your trip. Knowing what you will be shooting long before you arrive to the location.</li>
                                    <li>Gear overview. How to not overload yourself with equipment.</li>
                                    <li>Understanding Light (Golden Hour, Blue Hour, Sunrise and Sunset)</li>
                                    <li>Bracketing and multiple exposure blending</li>
                                    <li>Shooting in challenging weather or high contrast conditions (rain, ice, snow, water).</li>
                                    <li>Dealing with Sun Flare and shooting against the sun</li>
                                </ul>

                                <div class="splashfolioButtonBlock">
                                    <div class="splashfolioButton__in button-gray">
                                        <div class="splashfolioButton button-more">more</div>
                                    </div>
                                </div>-->
                            </div>

                            <div class="tripPlanning__right">
                                <div class="tripPlanning__img" style="background: url('img/DSC_9860.jpg') no-repeat center / cover"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tripPlanning_item">
                    <div class="tripPlanning_head">
                        <h4>Composition, trip planning, gear and more.</h4>
                    </div>

                    <div class="tripPlanning_block_in">
                        <div class="tripPlanning_block">
                            <div class="tripPlanning__left">
                                <ul>
                                    <li>Deep dive into landscape photography with numerous samples and tips.</li>
                                    <li>All about filters: polarize, neutral density, gradual density, circular and more.</li>
                                    <li>The art of seeing. Finding locations that give a unique perspective of a known landmark</li>
                                    <li>Planning your trip. Knowing what you will be shooting long before you arrive to the location.</li>
                                    <li>Gear overview. How to not overload yourself with equipment.</li>
                                    <li>Understanding Light (Golden Hour, Blue Hour, Sunrise and Sunset)</li>
                                    <li>Bracketing and multiple exposure blending</li>
                                    <!-- <li>Shooting in challenging weather or high contrast conditions (rain, ice, snow, water).</li>
                                     <li>Dealing with Sun Flare and shooting against the sun</li>
                                     <li>Building Panoramic images and learning about nodal points for impeccable panoramas. Vertical panoramas vs horizontal.</li>
                                     <li>The correct approach to long exposure. Blurring water and skies with long exposures</li>
                                     <li>Introduction to astrophotography.</li>
                                     <li>Master exposure and Histograms.</li>
                                     <li>Shooting in challenging weather or high contrast conditions (rain, ice, snow, water).</li>
                                     <li>Dealing with Sun Flare and shooting against the sun</li>-->
                                </ul>

                                <!--<ul class="more_block">
                                    <li>Planning your trip. Knowing what you will be shooting long before you arrive to the location.</li>
                                    <li>Gear overview. How to not overload yourself with equipment.</li>
                                    <li>Understanding Light (Golden Hour, Blue Hour, Sunrise and Sunset)</li>
                                    <li>Bracketing and multiple exposure blending</li>
                                    <li>Shooting in challenging weather or high contrast conditions (rain, ice, snow, water).</li>
                                    <li>Dealing with Sun Flare and shooting against the sun</li>
                                </ul>

                                <div class="splashfolioButtonBlock">
                                    <div class="splashfolioButton__in button-gray">
                                        <div class="splashfolioButton button-more">more</div>
                                    </div>
                                </div>-->
                            </div>

                            <div class="tripPlanning__right">
                                <div class="tripPlanning__img" style="background: url('img/DSC_9860.jpg') no-repeat center / cover"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tripPlanning_item">
                    <div class="tripPlanning_head">
                        <h4>Composition, trip planning, gear and more.</h4>
                    </div>

                    <div class="tripPlanning_block_in">
                        <div class="tripPlanning_block">
                            <div class="tripPlanning__left">
                                <ul>
                                    <li>Deep dive into landscape photography with numerous samples and tips.</li>
                                    <li>All about filters: polarize, neutral density, gradual density, circular and more.</li>
                                    <li>The art of seeing. Finding locations that give a unique perspective of a known landmark</li>
                                    <li>Planning your trip. Knowing what you will be shooting long before you arrive to the location.</li>
                                    <li>Gear overview. How to not overload yourself with equipment.</li>
                                    <li>Understanding Light (Golden Hour, Blue Hour, Sunrise and Sunset)</li>
                                    <li>Bracketing and multiple exposure blending</li>
                                    <!-- <li>Shooting in challenging weather or high contrast conditions (rain, ice, snow, water).</li>
                                     <li>Dealing with Sun Flare and shooting against the sun</li>
                                     <li>Building Panoramic images and learning about nodal points for impeccable panoramas. Vertical panoramas vs horizontal.</li>
                                     <li>The correct approach to long exposure. Blurring water and skies with long exposures</li>
                                     <li>Introduction to astrophotography.</li>
                                     <li>Master exposure and Histograms.</li>
                                     <li>Shooting in challenging weather or high contrast conditions (rain, ice, snow, water).</li>
                                     <li>Dealing with Sun Flare and shooting against the sun</li>-->
                                </ul>

                                <!--<ul class="more_block">
                                    <li>Planning your trip. Knowing what you will be shooting long before you arrive to the location.</li>
                                    <li>Gear overview. How to not overload yourself with equipment.</li>
                                    <li>Understanding Light (Golden Hour, Blue Hour, Sunrise and Sunset)</li>
                                    <li>Bracketing and multiple exposure blending</li>
                                    <li>Shooting in challenging weather or high contrast conditions (rain, ice, snow, water).</li>
                                    <li>Dealing with Sun Flare and shooting against the sun</li>
                                </ul>

                                <div class="splashfolioButtonBlock">
                                    <div class="splashfolioButton__in button-gray">
                                        <div class="splashfolioButton button-more">more</div>
                                    </div>
                                </div>-->
                            </div>

                            <div class="tripPlanning__right">
                                <div class="tripPlanning__img" style="background: url('img/DSC_9860.jpg') no-repeat center / cover"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="beforeAfter">
        <div class="container">
            <div class="h2"><h2>Before and after</h2></div>
            <div class="description__center">
                <div class="description">Use the sliders below to view before and after photo created within this tutorial on your computer or mobile device. This is a composite image, so multiple files are used to create one final image.</div>
            </div>

            <div id="beforeAfter__slider">
                <ul>
                    <li data-index="1">
                        <div class="cd-image-container">
                            <img src="/img/before_after/photo_after.jpg" alt="">
                            <span class="cd-image-label" data-type="original">After Edits</span>

                            <div class="cd-resize-img">
                                <img src="/img/before_after/photo_before.jpg" alt="">
                                <span class="cd-image-label" data-type="modified">Original</span>
                            </div>

                            <div class="cd-handle"></div>
                        </div>
                    </li>
                    <li data-index="2">
                        <div class="cd-image-container">
                            <img src="/img/before_after/photo_after_2.jpg" alt="">
                            <span class="cd-image-label" data-type="original">After Edits</span>

                            <div class="cd-resize-img">
                                <img src="/img/before_after/photo_before_2.jpg" alt="">
                                <span class="cd-image-label" data-type="modified">Original</span>
                            </div>

                            <div class="cd-handle"></div>
                        </div>
                    </li>
                    <li data-index="3">
                        <div class="cd-image-container">
                            <img src="/img/before_after/photo_after_3.jpg" alt="">
                            <span class="cd-image-label" data-type="original">After Edits</span>

                            <div class="cd-resize-img">
                                <img src="/img/before_after/photo_before_3.jpg" alt="">
                                <span class="cd-image-label" data-type="modified">Original</span>
                            </div>

                            <div class="cd-handle"></div>
                        </div>
                    </li>
                </ul>

                <div id="studentsGallery__arrow" class="display-none">
                    <div class="prev"></div>
                    <div class="next"></div>
                </div>
            </div>

            <div id="beforeAfter__pagin" class="display-none"></div>

            <div class="discountVideo__price">
                <div class="splashfolioButton button-green">Buy tutorial now</div>
                <div class="priceBlock">
                    <span>$1200</span>
                    <span>$3200</span>
                </div>
            </div>
        </div>
    </div>


    <div id="aboutTeacher">
        <div class="container text-align-center">
            <!--<div class="aboutTeacher__img" style="background: url('img/tutorials/tyler-nix.jpg') no-repeat center / cover"></div>-->
            <div class="h2"><h2>About Teacher</h2></div>

            <div class="aboutTeacher__block center-block">
                <div class="aboutTeacher__blockLeft">
                    <div class="aboutTeacher__avatar" style="background: url('/img/ourPhotographers/userpic-2.png') no-repeat center / cover">
                    </div>

                    <div class="aboutTeacher__name">Chris Knight</div>
                    <div class="aboutTeacher__job">Commercial Photographer</div>
                </div>

                <div class="aboutTeacher__blockRight">
                    <div class="aboutTeacher__video" style="background: url('/img/tutorials/teacher-video.jpg') no-repeat center / cover">
                        <div class="headerPlay" style="background: url('/img/tutorials/play-mini.svg') no-repeat center / cover"></div>
                    </div>
                </div>
            </div>
            <div class="description">
                <p>Chris Knight was born in Wiesbaden, Germany and hardened by the sweaty, nearly chewable, humidity of Florida. He combines his unconditional love of art history with his conditional love of technology, topping it off with a flare for the cinematic and an uncompromising eye for detail. His work has appeared in or on Vogue, People, MSNBC, ABC, Ocean Drive, GQ and others.</p>
                <p>He combines his unconditional love of art history with his conditional love of technology, topping it off with a flare for the cinematic and an uncompromising eye for detail. His work has appeared in or on Vogue, People, MSNBC, ABC, Ocean Drive, GQ and others.</p>
            </div>
            <div class="splashfolioButtonBlock discountVideoButton display-block">
                <div class="splashfolioButton__in button-gray">
                    <div class="splashfolioButton">more</div>
                </div>
            </div>
            <!--<div class="aboutTeacher__brand" style="background: url('img/tutorials/brand.png') no-repeat center / cover"></div>-->

        </div>
    </div>


    <div id="teacherGeart">
        <div class="container">
            <div class="teacherGeartBlock">
                <div class="teacherGeart__item">
                    <div class="teacherGeart__title camera">Camera</div>

                    <div class="teacherGeart__itemBlock">
                        <div class="teacherGeart__item_in">
                            <div class="teacherGeart__item_list">
                                <div class="teacherGeart__item_img">
                                    <img class="" src="/img/tutorials/teacherGeart/lumix.png">
                                </div>
                                <div class="teacherGeart__item_desc">
                                    <div rel="1" class="teacherGeart__caption">Panasonic Lumix G9</div>
                                    <div rel="1" class="teacherGeart__text">Main camera, with its 6.5 stops of in-body stabilisation, it’s ideal for handheld shooting, and the USB charging option made it ideal for this trip.</div>

                                    <div class="splashfolioButtonBlock">
                                        <div class="splashfolioButton__in button-gray">
                                            <div class="splashfolioButton button-more">See on amazon.com</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="teacherGeart__item_list">
                                <div class="teacherGeart__item_img">
                                    <img class="" src="/img/tutorials/teacherGeart/sony.png">
                                </div>
                                <div class="teacherGeart__item_desc">
                                    <div rel="1" class="teacherGeart__caption">Sony Alpha ILCE-6000</div>
                                    <div rel="1" class="teacherGeart__text">Main camera, with its 6.5 stops of in-body stabilisation, it’s ideal for handheld shooting, and the USB charging option made it ideal for this trip.</div>
                                    <div class="splashfolioButtonBlock">
                                        <div class="splashfolioButton__in button-gray">
                                            <div class="splashfolioButton button-more">See on amazon.com</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="teacherGeart__item_list">
                                <div class="teacherGeart__item_img">
                                    <img class="" src="/img/tutorials/teacherGeart/canon.png">
                                </div>
                                <div class="teacherGeart__item_desc">
                                    <div rel="1" class="teacherGeart__caption">Canon EOS 5D Mark III</div>
                                    <div rel="1" class="teacherGeart__text">Main camera, with its 6.5 stops of in-body stabilisation, it’s ideal for handheld shooting, and the USB charging option made it ideal for this trip.</div>
                                    <div class="splashfolioButtonBlock">
                                        <div class="splashfolioButton__in button-gray">
                                            <div class="splashfolioButton button-more">See on amazon.com</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="teacherGeart__item_list">
                                <div class="teacherGeart__item_img">
                                    <img class="" src="/img/tutorials/teacherGeart/samsung.png">
                                </div>
                                <div class="teacherGeart__item_desc">
                                    <div rel="1" class="teacherGeart__caption">Samsung NX1</div>
                                    <div rel="1" class="teacherGeart__text">Main camera, with its 6.5 stops of in-body stabilisation, it’s ideal for handheld shooting, and the USB charging option made it ideal for this trip.</div>
                                    <div class="splashfolioButtonBlock">
                                        <div class="splashfolioButton__in button-gray">
                                            <div class="splashfolioButton button-more">See on amazon.com</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="teacherGeart__item">
                    <div class="teacherGeart__title lenses">Lenses</div>

                    <div class="teacherGeart__itemBlock">
                        <div class="teacherGeart__item_in">
                            <div class="teacherGeart__item_list">
                                <div class="teacherGeart__item_img">
                                    <img class="" src="/img/tutorials/teacherGeart/lumix.png">
                                </div>
                                <div class="teacherGeart__item_desc">
                                    <div rel="1" class="teacherGeart__caption">Panasonic Lumix 12-35mm f/2.8</div>
                                    <div rel="1" class="teacherGeart__text">My go-to telephoto lens for travelling. A super compact 70-200mm f/2.8 equivalent, that’s always in my bag.</div>

                                    <div class="splashfolioButtonBlock">
                                        <div class="splashfolioButton__in button-gray">
                                            <div class="splashfolioButton button-more">See on amazon.com</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="teacherGeart__item_list">
                                <div class="teacherGeart__item_img">
                                    <img class="" src="/img/tutorials/teacherGeart/sony.png">
                                </div>
                                <div class="teacherGeart__item_desc">
                                    <div rel="1" class="teacherGeart__caption">Panasonic Lumix 35-100mm f/2.8</div>
                                    <div rel="1" class="teacherGeart__text">My go-to telephoto lens for travelling. A super compact 70-200mm f/2.8 equivalent, that’s always in my bag.</div>
                                    <div class="splashfolioButtonBlock">
                                        <div class="splashfolioButton__in button-gray">
                                            <div class="splashfolioButton button-more">See on amazon.com</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="teacherGeart__item_list">
                                <div class="teacherGeart__item_img">
                                    <img class="" src="/img/tutorials/teacherGeart/canon.png">
                                </div>
                                <div class="teacherGeart__item_desc">
                                    <div rel="1" class="teacherGeart__caption">Panasonic Leica 42.5mm f/1.2</div>
                                    <div rel="1" class="teacherGeart__text">My go-to telephoto lens for travelling. A super compact 70-200mm f/2.8 equivalent, that’s always in my bag.</div>
                                    <div class="splashfolioButtonBlock">
                                        <div class="splashfolioButton__in button-gray">
                                            <div class="splashfolioButton button-more">See on amazon.com</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="teacherGeart__item_list">
                                <div class="teacherGeart__item_img">
                                    <img class="" src="/img/tutorials/teacherGeart/samsung.png">
                                </div>
                                <div class="teacherGeart__item_desc">
                                    <div rel="1" class="teacherGeart__caption">Canon 14mm f/2.8 L Series</div>
                                    <div rel="1" class="teacherGeart__text">My go-to telephoto lens for travelling. A super compact 70-200mm f/2.8 equivalent, that’s always in my bag.</div>
                                    <div class="splashfolioButtonBlock">
                                        <div class="splashfolioButton__in button-gray">
                                            <div class="splashfolioButton button-more">See on amazon.com</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="teacherGeart__item">
                    <div class="teacherGeart__title accesories">Accesories</div>

                    <div class="teacherGeart__itemBlock">
                        <div class="teacherGeart__item_in">
                            <div class="teacherGeart__item_list">
                                <div class="teacherGeart__item_img">
                                    <img class="" src="/img/tutorials/teacherGeart/lumix.png">
                                </div>
                                <div class="teacherGeart__item_desc">
                                    <div rel="1" class="teacherGeart__caption">Manfrotto BeFree Travel Tripod</div>
                                    <div rel="1" class="teacherGeart__text">My go-to telephoto lens for travelling. A super compact 70-200mm f/2.8 equivalent, that’s always in my bag.</div>

                                    <div class="splashfolioButtonBlock">
                                        <div class="splashfolioButton__in button-gray">
                                            <div class="splashfolioButton button-more">See on amazon.com</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="teacherGeart__item_list">
                                <div class="teacherGeart__item_img">
                                    <img class="" src="/img/tutorials/teacherGeart/sony.png">
                                </div>
                                <div class="teacherGeart__item_desc">
                                    <div rel="1" class="teacherGeart__caption">Macbook Pro 15” Retina</div>
                                    <div rel="1" class="teacherGeart__text">My go-to telephoto lens for travelling. A super compact 70-200mm f/2.8 equivalent, that’s always in my bag.</div>
                                    <div class="splashfolioButtonBlock">
                                        <div class="splashfolioButton__in button-gray">
                                            <div class="splashfolioButton button-more">See on amazon.com</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="teacherGeart__item_list">
                                <div class="teacherGeart__item_img">
                                    <img class="" src="/img/tutorials/teacherGeart/canon.png">
                                </div>
                                <div class="teacherGeart__item_desc">
                                    <div rel="1" class="teacherGeart__caption">Atomos Ninja Flame</div>
                                    <div rel="1" class="teacherGeart__text">My go-to telephoto lens for travelling. A super compact 70-200mm f/2.8 equivalent, that’s always in my bag.</div>
                                    <div class="splashfolioButtonBlock">
                                        <div class="splashfolioButton__in button-gray">
                                            <div class="splashfolioButton button-more">See on amazon.com</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div id="relatedProducts">
        <div class="container">
            <div class="h2"><h2>Related Products</h2></div>

            <div id="relatedProducts__slider">
                <ul>
                    <li>
                        <div class="photoItem" style="background: url('/img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                            <div class="photoItemBlock">
                                <div class="featured">featured</div>
                                <h4>Creative Photoshop Techniques</h4>
                                <div class="arrow"></div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="photoItem" style="background: url('/img/ourPhotographers/DSC_0871_1-Pano.jpg') no-repeat center / cover">
                            <div class="photoItemBlock">
                                <div class="featured">featured</div>
                                <h4>Creative Photoshop Techniques</h4>
                                <div class="arrow"></div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="photoItem" style="background: url('/img/ourPhotographers/DSC_0871_1-Pano.jpg') no-repeat center / cover">
                            <div class="photoItemBlock">
                                <div class="featured">featured</div>
                                <h4>Creative Photoshop Techniques</h4>
                                <div class="arrow"></div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="photoItem" style="background: url('/img/ourPhotographers/DSC_3132_F.jpg') no-repeat center / cover">
                            <div class="photoItemBlock">
                                <div class="featured">featured</div>
                                <h4>Creative Photoshop Techniques</h4>
                                <div class="arrow"></div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="photoItem" style="background: url('/img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                            <div class="photoItemBlock">
                                <div class="featured">featured</div>
                                <h4>Creative Photoshop Techniques</h4>
                                <div class="arrow"></div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="photoItem" style="background: url('/img/ourPhotographers/DSC_4216.jpg') no-repeat center / cover">
                            <div class="photoItemBlock">
                                <div class="featured">featured</div>
                                <h4>Creative Photoshop Techniques</h4>
                                <div class="arrow"></div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="photoItem" style="background: url('/img/ourPhotographers/DSC_7893.jpg') no-repeat center / cover">
                            <div class="photoItemBlock">
                                <div class="featured">featured</div>
                                <h4>Creative Photoshop Techniques</h4>
                                <div class="arrow"></div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="photoItem" style="background: url('/img/ourPhotographers/DSC_1349_Panorama2_16bit_100_67.jpg') no-repeat center / cover">
                            <div class="photoItemBlock">
                                <div class="featured">featured</div>
                                <h4>Creative Photoshop Techniques</h4>
                                <div class="arrow"></div>
                            </div>
                        </div>
                    </li>
                </ul>

                <div id="studentsGallery__arrow" class="display-none">
                    <div class="prev"></div>
                    <div class="next"></div>
                </div>
            </div>

            <div class="splashfolioButtonBlock">
                <div class="splashfolioButton__in">
                    <div class="splashfolioButton button-orange relatedProducts-open">See all</div>
                </div>
            </div>
        </div>
    </div>



    <div id="testimonials-tutorial">
        <div class="container">
            <div class="h2"><h2>Reviews</h2></div>

            <div class="testimonials__total">234 total</div>

            <div class="testimonialsButton">
                <div class="splashfolioButton__in">
                    <div class="splashfolioButton button-gray">See all</div>
                </div>
                <div class="splashfolioButton__in">
                    <div class="splashfolioButton button-gray">Write a review</div>
                </div>
            </div>

            <div class="testimonials__block center-block">
                <div class="testimonials__item">
                    <div class="testimonials__author">
                        <div class="testimonials__avatar" style="background: url('/img/ourPhotographers/user1.png') no-repeat center / cover">
                        </div>
                        <div class="testimonials__head">
                            <div class="testimonials__name">
                                Alfred Jefferson
                                <span>Photographer</span>
                            </div>

                            <div class="testimonials__star">
                                <div class="testimonials__star-item active">  </div>
                                <div class="testimonials__star-item active">  </div>
                                <div class="testimonials__star-item active">  </div>
                                <div class="testimonials__star-item">  </div>
                                <div class="testimonials__star-item">  </div>
                            </div>
                        </div>
                    </div>

                    <div class="description">
                        The instructor goes through and compares lenses, explained his locations and shows its extremely valuable and sought out editing skills. The instructor goes …
                        <a href="#">more</a>
                    </div>
                </div>

                <div class="testimonials__item">
                    <div class="testimonials__author">
                        <div class="testimonials__avatar" style="background: url('/img/ourPhotographers/user1.png') no-repeat center / cover"></div>
                        <div class="testimonials__head">
                            <div class="testimonials__name">
                                Nicole Knipes
                                <span>Digital Designer</span>
                            </div>

                            <div class="testimonials__star">
                                <div class="testimonials__star-item active">  </div>
                                <div class="testimonials__star-item active">  </div>
                                <div class="testimonials__star-item active">  </div>
                                <div class="testimonials__star-item">  </div>
                                <div class="testimonials__star-item">  </div>
                            </div>
                        </div>
                    </div>

                    <div class="description">
                        This course is exactly what I needed to complete my photography skill set. The instructor goes through and compares lenses, explained his locations.
                    </div>
                </div>

                <div class="testimonials__item">
                    <div class="testimonials__author">
                        <div class="testimonials__avatar" style="background: url('/img/ourPhotographers/user1.png') no-repeat center / cover"></div>

                        <div class="testimonials__head">
                            <div class="testimonials__name">
                                Connor Siedow
                                <span>Creative Director</span>
                            </div>

                            <div class="testimonials__star">
                                <div class="testimonials__star-item active">  </div>
                                <div class="testimonials__star-item active">  </div>
                                <div class="testimonials__star-item active">  </div>
                                <div class="testimonials__star-item">  </div>
                                <div class="testimonials__star-item">  </div>
                            </div>
                        </div>
                    </div>

                    <div class="description">
                        Seriously worth every penny and is worth way more than $ could by! I found it essential for the next steps on my photography career.
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="studentsGallery">
        <div class="container">
            <div class="h2"><h2>Student Work</h2></div>
            <div class="description">Sign Up To Get Our Biggest Sales
                And Early Access To Tutorials</div>

            <div id="studentsGallery__slider">
                <ul>
                    <li>
                        <div class="studentsGallery__item" style="background: url('/img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                            <div class="studentsGallery__name">Connor Siedow</div>
                            <div class="studentsGallery__itemLike">
                                <div class="studentsGallery__Like"></div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="studentsGallery__item" style="background: url('/img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                            <div class="studentsGallery__itemLike">
                                <div class="studentsGallery__Like"></div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="studentsGallery__item" style="background: url('/img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                            <div class="studentsGallery__itemLike">
                                <div class="studentsGallery__Like active"></div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="studentsGallery__item" style="background: url('/img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                            <div class="studentsGallery__itemLike">
                                <div class="studentsGallery__Like"></div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="studentsGallery__item" style="background: url('/img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                            <div class="studentsGallery__itemLike">
                                <div class="studentsGallery__Like"></div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="studentsGallery__item" style="background: url('/img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                            <div class="studentsGallery__itemLike">
                                <div class="studentsGallery__Like"></div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="studentsGallery__item" style="background: url('/img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                            <div class="studentsGallery__itemLike">
                                <div class="studentsGallery__Like"></div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="studentsGallery__item" style="background: url('/img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                            <div class="studentsGallery__itemLike">
                                <div class="studentsGallery__Like"></div>
                            </div>
                        </div>
                    </li>
                </ul>

                <div id="studentsGallery__arrow" class="display-none">
                    <div class="prev"></div>
                    <div class="next"></div>
                </div>
            </div>

            <div class="splashfolioButtonBlock">
                <div class="splashfolioButton__in">
                    <div class="splashfolioButton button-orange">Add photo</div>
                </div>
            </div>
        </div>
    </div>


    <div id="instagram">
        <div class="container">
            <div class="h2"><h2>Teacher’s
                    Instagram Feed</h2></div>
            <div class="instagram__img">
                <div class="instagram__imgBlock">
                    <div class="instagram__item" style="background: url('/img/ourPhotographers/DSC_0871_1-Pano.jpg') no-repeat center / cover"></div>
                    <div class="instagram__item" style="background: url('/img/ourPhotographers/DSC04537.jpg') no-repeat center / cover"></div>
                </div>

                <div class="instagram__imgBlock">
                    <div class="instagram__item" style="background: url('/img/ourPhotographers/DSC_0871_1-Pano.jpg') no-repeat center / cover"></div>
                    <div class="instagram__item" style="background: url('/img/ourPhotographers/DSC_9860.jpg') no-repeat center / cover"></div>
                </div>

                <div class="instagram__imgBlock">
                    <div class="instagram__item" style="background: url('/img/ourPhotographers/DSC_0871_1-Pano.jpg') no-repeat center / cover"></div>
                    <div class="instagram__item" style="background: url('/img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover"></div>
                </div>
            </div>
        </div>
    </div>


    <div id="purchaseNow">
        <div class="container">
            <div class="purchaseNowBlock">
                <div class="h2"><h2><span>Purchase</span> Tutorial Now</h2></div>
                <div class="description">An extensive 10+ hours tutorial on landscape photography. Full workflow: from the on-location capture to post-processing technique.</div>
                <div class="discountVideo__price">
                    <div class="splashfolioButton__in">
                        <div class="splashfolioButton button-green">
                            <div class="display-none">Buy tutorial now</div>
                            <div class="display-block">Purchase</div>
                        </div>
                    </div>
                    <div class="priceBlock">
                        <span>$1200</span>
                        <span>$3200</span>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="freeTest">
        <div class="container">
            <div class="h2"><h2><span>Test Your</span> Knowlenge. Free!</h2></div>

            <div class="freeTest_block">
                <div class="freeTest_item one">
                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.?
                </div>

                <div class="freeTest_item two">
                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.?
                </div>

                <div class="freeTest_item three">
                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.?
                </div>
            </div>

            <div class="splashfolioButtonBlock">
                <div class="splashfolioButton__in">
                    <div class="splashfolioButton button-orange">Take a test</div>
                </div>
            </div>
        </div>
    </div>



    <div id="getDiscount">
        <div class="container">
            <div class="h2"><h2>Get 10% discount</h2></div>
            <div class="description">Subscribe to our newsletter and get discount for your first purchase</div>

            <div class="discountForm">
                <form action="" class="center-block">

                    <div class="default-form-input">
                        <input name="name" type="text" placeholder="Your Name">
                    </div>

                    <div class="default-form-input">
                        <input name="email" type="email" placeholder="Email Address">
                    </div>

                    <div class="splashfolioButton__in">
                        <button name="submit_button" class="splashfolioButton button-orange">Subscribe</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

</div>