<div id="center">
    <div id="testimonials">
        <div class="container">
            <div class="h2"><h2><?php echo Yii::t('site', 'Reviews'); ?></h2></div>

            <div class="testimonials__total">
                <?=$reviews_count?> <?php echo Yii::t('site', 'total'); ?>
            </div>

            <div class="testimonialsButton">
                <div class="splashfolioButton__in">
                    <a href="/reviews/write" class="splashfolioButton button-gray">
                        <?php echo Yii::t('site', 'Write a review'); ?>
                    </a>
                </div>
            </div>

            <div class="testimonials__block center-block">
                <?php if (!empty($reviews)): ?>
                    <?php foreach ($reviews as $review): ?>
                        <div class="testimonials__item">
                            <div class="testimonials__author">
                                <div class="testimonials__avatar" style="background: url('img/ourPhotographers/user1.png') no-repeat center / cover">
                                </div>
                                <div class="testimonials__head">
                                    <div class="testimonials__name">
                                        <?=$review->name?>
                                        <span><?=$review->profession?></span>
                                    </div>

                                    <div class="testimonials__star">
                                        <?php for ($i = 1; $i <= 5; $i++): ?>
                                            <?php $active = ($i <= $review->stars) ? 'active' : '';?>
                                            <div class="testimonials__star-item <?=$active?>"></div>
                                        <?php endfor;?>
                                    </div>
                                </div>
                            </div>

                            <div class="description">
                                <?=mb_substr($review->review_text, 0 , 160);?><?php if (strlen($review->review_text) > 160): ?>...
                                    <a href="#">more</a>
                                <?php endif;?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>