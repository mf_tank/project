<div id="header">
    <!--<div class="container">-->
    <?php echo $this->render('/blocks/_banners', [
        'banners' => $banners
    ]);?>
    <!--</div>-->
</div>
<div id="center">
    <div id="ourPhotographers">
        <div class="container">
            <div class="ourPhotographers__block">
                <div class="h2"><h2><?php echo Yii::t('site', $promo_our_photographers_title); ?></h2></div>
                <div class="description"><?php echo Yii::t('site', $promo_our_photographers_description); ?></div>
                <div class="ourPhotographers__in">
                    <div class="ourPhotographers__brand">
                        <div class="ourPhotographers__brandItem"></div>
                        <div class="ourPhotographers__brandItem"></div>
                        <div class="ourPhotographers__brandItem"></div>
                        <div class="ourPhotographers__brandItem"></div>
                        <div class="ourPhotographers__brandItem"></div>
                        <div class="ourPhotographers__brandItem"></div>
                    </div>
                    <div class="ourPhotographers__brand">
                        <div class="ourPhotographers__brandItem"></div>
                        <div class="ourPhotographers__brandItem"></div>
                        <div class="ourPhotographers__brandItem"></div>
                        <div class="ourPhotographers__brandItem"></div>
                        <div class="ourPhotographers__brandItem"></div>
                    </div>
                </div>
            </div>

            <div class="ourPhotographers_imgBlock center-block">

                <?php if (!empty($our_photographers)): ?>
                    <?php foreach ($our_photographers as $photographer): ?>
                        <div class="ourPhotographers__item">
                            <div class="ourPhotographers__topic" style="background: url('<?= $photographer->image ? $photographer->image->img : '/img/ourPhotographers/DSC_0221.jpg'?>') no-repeat center / cover">
                                <div class="ourPhotographers__topicBlock">
                                    <div class="ourPhotographers__topicBlock_in">
                                        <div class="ourPhotographers__avatar" style="background: url('img/ourPhotographers/user1.png') no-repeat center / cover"></div>
                                        <h4><?=$photographer->user->username?></h4>
                                    </div>
                                    <h2><?=$photographer->title?></h2>

                                    <div class="description">
                                        <?=$photographer->description?>
                                    </div>

                                    <div class="ourPhotographers__Button">
                                        <div class="splashfolioButton__in">
                                            <div class="splashfolioButton button-gray">Learn More</div>
                                        </div>
                                        <div class="splashfolioButton__in">
                                            <div class="splashfolioButton button-orange">Play Trailer</div>
                                        </div>
                                    </div>

                                    <div class="arrow"></div>
                                </div>
                            </div>
                            <div class="ourPhotographers__list">
                                <div class="photoItem" style="background: url('img/ourPhotographers/DSC_1349_Panorama2_16bit_100_67.jpg') no-repeat center / cover">
                                    <div class="photoItemBlock">
                                        <h4>Natural Light Portraits</h4>
                                        <div class="arrow"></div>
                                    </div>
                                </div>

                                <div class="photoItem" style="background: url('img/ourPhotographers/DSC_0221.jpg') no-repeat center / cover">
                                    <div class="photoItemBlock">
                                        <div class="featured">featured</div>
                                        <h4>Lifestyle Photography</h4>
                                        <div class="arrow"></div>
                                    </div>
                                </div>

                                <div class="photoItem" style="background: url('img/ourPhotographers/DSC_3448.jpg') no-repeat center / cover">
                                    <div class="photoItemBlock">
                                        <h4>Fashion Photography</h4>
                                        <div class="arrow"></div>
                                    </div>
                                </div>

                                <div class="photoItem" style="background: url('img/ourPhotographers/DSC_7893.jpg') no-repeat center / cover">
                                    <div class="photoItemBlock">
                                        <div class="featured">featured</div>
                                        <h4>Lifestyle Photography</h4>
                                        <div class="arrow"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>

            <a href="/tutorials">
                <div class="splashfolioButtonBlock">
                    <div class="splashfolioButton__in">
                        <div class="splashfolioButton button-orange">
                            <?php echo Yii::t('site', $promo_our_photographers_button); ?>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>


    <div id="featuredProducts">
        <div class="container">
            <div class="h2"><h2><?php echo Yii::t('site', 'Featured'); ?></h2></div>
            <div class="featuredProducts__item center-block">
                <div class="photoItem" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                    <div class="photoItemBlock">
                        <div class="featured">featured</div>
                        <h4>Creative Photoshop Techniques</h4>
                        <div class="arrow"></div>
                    </div>
                </div>

                <div class="photoItem" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                    <div class="photoItemBlock">
                        <div class="featured">featured</div>
                        <h4>Creative Photoshop Techniques</h4>
                        <div class="arrow"></div>
                    </div>
                </div>

                <div class="photoItem" style="background: url('img/ourPhotographers/DSC_9860.jpg') no-repeat center / cover">
                    <div class="photoItemBlock">
                        <div class="featured hide">featured</div>
                        <h4>Creative Photoshop Techniques</h4>
                        <div class="arrow"></div>
                    </div>
                </div>

                <!--   <div class="photoItem" style="background: url('img/ourPhotographers/DSC_2436.jpg') no-repeat center / cover">
                       <div class="photoItemBlock">
                           <div class="featured">featured</div>
                           <h4>Creative Photoshop Techniques</h4>
                           <div class="arrow"></div>
                       </div>
                   </div>

                   <div class="photoItem" style="background: url('img/ourPhotographers/DSC_0871_1-Pano.jpg') no-repeat center / cover">
                       <div class="photoItemBlock">
                           <div class="featured">featured</div>
                           <h4>Creative Photoshop Techniques</h4>
                           <div class="arrow"></div>
                       </div>
                   </div>

                   <div class="photoItem" style="background: url('img/ourPhotographers/DSC_3132_F.jpg') no-repeat center / cover">
                       <div class="photoItemBlock">
                           <div class="featured">featured</div>
                           <h4>Creative Photoshop Techniques</h4>
                           <div class="arrow"></div>
                       </div>
                   </div>

                   <div class="photoItem" style="background: url('img/ourPhotographers/DSC_3132_F.jpg') no-repeat center / cover">
                       <div class="photoItemBlock">
                           <div class="featured">featured</div>
                           <h4>Creative Photoshop Techniques</h4>
                           <div class="arrow"></div>
                       </div>
                   </div>

                   <div class="photoItem" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                       <div class="photoItemBlock">
                           <div class="featured">featured</div>
                           <h4>Creative Photoshop Techniques</h4>
                           <div class="arrow"></div>
                       </div>
                   </div>

                   <div class="photoItem" style="background: url('img/ourPhotographers/DSC_4216.jpg') no-repeat center / cover">
                       <div class="photoItemBlock">
                           <div class="featured">featured</div>
                           <h4>Creative Photoshop Techniques</h4>
                           <div class="arrow"></div>
                       </div>
                   </div>

                   <div class="photoItem" style="background: url('img/ourPhotographers/DSC_7893.jpg') no-repeat center / cover">
                       <div class="photoItemBlock">
                           <div class="featured">featured</div>
                           <h4>Creative Photoshop Techniques</h4>
                           <div class="arrow"></div>
                       </div>
                   </div>

                   <div class="photoItem" style="background: url('img/ourPhotographers/DSC_1349_Panorama2_16bit_100_67.jpg') no-repeat center / cover">
                       <div class="photoItemBlock">
                           <div class="featured">featured</div>
                           <h4>Creative Photoshop Techniques</h4>
                           <div class="arrow"></div>
                       </div>
                   </div>

                   <div class="photoItem" style="background: url('img/ourPhotographers/DSC_1349_Panorama2_16bit_100_67.jpg') no-repeat center / cover">
                       <div class="photoItemBlock">
                           <div class="featured">featured</div>
                           <h4>Creative Photoshop Techniques</h4>
                           <div class="arrow"></div>
                       </div>
                   </div>-->
            </div>

            <div class="splashfolioButtonBlock">
                <div class="splashfolioButton__in">
                    <div class="splashfolioButton button-orange">See all</div>
                </div>
            </div>
        </div>
    </div>


    <?php echo $this->render('/blocks/_discount');?>


    <div id="testimonials">
        <div class="container">
            <div class="h2"><h2><?php echo Yii::t('site', 'Reviews'); ?></h2></div>

            <div class="testimonials__total">
                <?=$reviews_count?> <?php echo Yii::t('site', 'total'); ?>
            </div>

            <div class="testimonialsButton">
                <div class="splashfolioButton__in">
                    <a href="/reviews" class="splashfolioButton button-gray">
                        <?php echo Yii::t('site', 'See all'); ?>
                    </a>
                </div>
                <div class="splashfolioButton__in">
                    <a href="/reviews/write" class="splashfolioButton button-gray">
                        <?php echo Yii::t('site', 'Write a review'); ?>
                    </a>
                </div>
            </div>

            <div class="testimonials__block center-block">
                <?php if (!empty($reviews)): ?>
                    <?php foreach ($reviews as $review): ?>
                        <div class="testimonials__item">
                            <div class="testimonials__author">
                                <div class="testimonials__avatar" style="background: url('img/ourPhotographers/user1.png') no-repeat center / cover">
                                </div>
                                <div class="testimonials__head">
                                    <div class="testimonials__name">
                                        <?=$review->name?>
                                        <span><?=$review->profession?></span>
                                    </div>

                                    <div class="testimonials__star">
                                        <?php for ($i = 1; $i <= 5; $i++): ?>
                                            <?php $active = ($i <= $review->stars) ? 'active' : '';?>
                                            <div class="testimonials__star-item <?=$active?>"></div>
                                        <?php endfor;?>
                                    </div>
                                </div>
                            </div>

                            <div class="description">
                                <?=mb_substr($review->review_text, 0 , 160);?><?php if (strlen($review->review_text) > 160): ?>...
                                    <a href="#">more</a>
                                <?php endif;?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>


    <div id="studentsGallery">
        <div class="container">
            <div class="h2"><h2>Student Work</h2></div>
            <div class="description">Sign Up To Get Our Biggest Sales
                And Early Access To Tutorials</div>

            <div id="studentsGallery__slider">
                <ul>
                    <li>
                        <div class="studentsGallery__item" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                            <div class="studentsGallery__name">Connor Siedow</div>
                            <div class="studentsGallery__itemLike">
                                <div class="studentsGallery__Like"></div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="studentsGallery__item" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                            <div class="studentsGallery__itemLike">
                                <div class="studentsGallery__Like"></div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="studentsGallery__item" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                            <div class="studentsGallery__itemLike">
                                <div class="studentsGallery__Like active"></div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="studentsGallery__item" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                            <div class="studentsGallery__itemLike">
                                <div class="studentsGallery__Like"></div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="studentsGallery__item" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                            <div class="studentsGallery__itemLike">
                                <div class="studentsGallery__Like"></div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="studentsGallery__item" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                            <div class="studentsGallery__itemLike">
                                <div class="studentsGallery__Like"></div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="studentsGallery__item" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                            <div class="studentsGallery__itemLike">
                                <div class="studentsGallery__Like"></div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="studentsGallery__item" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                            <div class="studentsGallery__itemLike">
                                <div class="studentsGallery__Like"></div>
                            </div>
                        </div>
                    </li>
                </ul>

                <div id="studentsGallery__arrow" class="display-none">
                    <div class="prev"></div>
                    <div class="next"></div>
                </div>
            </div>

            <div class="splashfolioButtonBlock">
                <div class="splashfolioButton__in">
                    <div class="splashfolioButton button-orange">Add photo</div>
                </div>
            </div>
        </div>
    </div>


    <div id="instagram">
        <div class="container">
            <div class="h2"><h2>Instagram feed from our teachers</h2></div>
            <div class="instagram__img">
                <div class="instagram__imgBlock">
                    <div class="instagram__item" style="background: url('img/ourPhotographers/DSC_0871_1-Pano.jpg') no-repeat center / cover"></div>
                    <div class="instagram__item" style="background: url('img/ourPhotographers/DSC04537.jpg') no-repeat center / cover"></div>
                </div>

                <div class="instagram__imgBlock">
                    <div class="instagram__item" style="background: url('img/ourPhotographers/DSC_0871_1-Pano.jpg') no-repeat center / cover"></div>
                    <div class="instagram__item" style="background: url('img/ourPhotographers/DSC_9860.jpg') no-repeat center / cover"></div>
                </div>

                <div class="instagram__imgBlock">
                    <div class="instagram__item" style="background: url('img/ourPhotographers/DSC_0871_1-Pano.jpg') no-repeat center / cover"></div>
                    <div class="instagram__item" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover"></div>
                </div>
            </div>
        </div>
    </div>


    <div id="learn">
        <div class="container">
            <div class="h2"><h2>Learn from the best</h2></div>
            <div class="description">Visit our blog for a deeper dive into all things MasterClass</div>

            <div class="learn__item center-block">
                <div class="photoItem" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                    <div class="photoItemBlock">
                        <div class="featured">featured</div>

                        <h4>Creative Photoshop Techniques</h4>

                        <div class="arrow"></div>

                        <div class="learnItem__Button">
                            <div class="splashfolioButton__in">
                                <div class="splashfolioButton button-gray">Read</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="photoItem" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                    <div class="photoItemBlock">
                        <div class="featured">featured</div>

                        <h4>Creative Photoshop Techniques</h4>

                        <div class="arrow"></div>

                        <div class="learnItem__Button">
                            <div class="splashfolioButton__in">
                                <div class="splashfolioButton button-gray">Read</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="photoItem" style="background: url('img/ourPhotographers/DSC_9860.jpg') no-repeat center / cover">
                    <div class="photoItemBlock">
                        <div class="featured">featured</div>

                        <h4>Creative Photoshop Techniques</h4>

                        <div class="arrow"></div>

                        <div class="learnItem__Button">
                            <div class="splashfolioButton__in">
                                <div class="splashfolioButton button-gray">Read</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="splashfolioButtonBlock">
                <div class="splashfolioButton__in">
                    <div class="splashfolioButton button-orange">Read all</div>
                </div>
            </div>
        </div>
    </div>


    <?php echo $this->render('/blocks/_socials', [
            'socials' => $socials
    ]);?>
</div>
