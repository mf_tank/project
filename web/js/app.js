/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(1);


/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__less_app_less__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__less_app_less___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__less_app_less__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__script__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__script___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__script__);





/***/ }),
/* 2 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 3 */
/***/ (function(module, exports) {

$(function(){
	function oneRunFunc() {
		let destroyFlag = true,
			reloadFlag = true;

		this.destroy = function(func){
			reloadFlag = true;
			if (destroyFlag) {
				func();
			}
			destroyFlag = false;
		};
		this.reload = function(func){
			destroyFlag = true;
			if(reloadFlag){
				func();
			}
			reloadFlag = false;
		}
	}

	var oneRun = new oneRunFunc();
	var oneRunSl = new oneRunFunc();
	var oneRunSl2 = new oneRunFunc();
	var oneRunSl3 = new oneRunFunc();

	/*********************************************************************/
	var lastScrollTop = 0;
	var topMenu = $('#top_menu');

	$(window).on('scroll init', function(){
		var st = $(this).scrollTop();

		if (st > lastScrollTop) {
			topMenu.addClass('fixed');
		}

		if (st === 0) {
			topMenu.removeClass('fixed');
		}

		lastScrollTop = st;
	}).trigger('init');
	/*********************************************************************/
	var header_carousel = $('#header_carousel');

	function projectSlider(project_slider) {
		if (project_slider.length) {
			project_slider.each(function () {
				var prev = project_slider.find('.prev'),
					next = project_slider.find('.next'),
					pagin = project_slider.find('.pagin');

				header_carousel.carouFredSel({
					responsive: true,
					auto: false,
					pagination: pagin,
					prev: prev,
					next: next,
					items: {
						visible: 1
					},
					scroll: {
						items: 1
					},
					swipe: {
						onTouch: true
					}
				});
			});
		}
	}
	/*********************************************************************/
	var slFotoPro = $('#relatedProducts__slider');

	function slFotosPro(visible){
		if (slFotoPro.length) {
			var prev      = slFotoPro.find('.prev'),
				next        = slFotoPro.find('.next'),
				sliderOurUl = slFotoPro.find('ul');

			sliderOurUl.carouFredSel({
				width: '100%',
				responsive: true,
				auto: false,
				prev: prev,
				next: next,
				items: {
					height : "auto",
					visible: visible
				},
				scroll: {
					items: visible
				},
				swipe: {
					onMouse: true,
					onTouch: true
				}
			});

			sliderOurUl.trigger('updateSizes');
		}
	}
	/*********************************************************************/
	var slFotoBef = $('#beforeAfter__slider');

	if (slFotoBef.length) {
		var prev      = slFotoBef.find('.prev'),
			next        = slFotoBef.find('.next'),
			sliderOurUl = slFotoBef.find('ul');

		sliderOurUl.carouFredSel({
			width: '100%',
			responsive: true,
			auto: false,
			prev: prev,
			next: next,
			items: {
				height : "auto",
				visible: 1
			},
			scroll: {
				items: 1
			},
			swipe: {
				onMouse: true,
				onTouch: true
			},
			pagination: {
				container: '#beforeAfter__pagin',
				anchorBuilder: false
			},
			onCreate: function(data) {
				var listEl = data.items.prevObject;
				var pagin = '';
				var width = (100 / listEl.length) + '%';

				for (let i = 0; i < listEl.length; i++) {
					pagin += '<a style="width: '+width+'" rel="'+listEl[i].dataset.index+'" href="#"></a>';
				}

				$('#beforeAfter__pagin').html(pagin);
			}
		});

		$('#beforeAfter__pagin a').on('click', function() {
			if (sl.triggerHandler('isScrolling')) {
				return false;
			}

			sl.trigger('slideTo', [ '[data-index="' + $(this).attr('rel') + '"]' ]);
		});

		sliderOurUl.trigger('updateSizes');
	}
	/*********************************************************************/
	$('.teacherGeart__right .teacherGeart__contentlist').on('click', function() {
		$(this).parents('.teacherGeart__right').find('.active').removeClass('active');
		$(this).parents('.teacherGeart__right').find('.teacherGeart__text').hide();

		$(this).addClass('active');
		$('.teacherGeart__text[rel="' + $(this).attr('rel') + '"]').fadeIn();
	});
	/*********************************************************************/
	$('.teacherGeart__left .teacherGeart__modelBlock').on('click', function() {
		if ($(this).hasClass('open')) {
			$(this).removeClass('open').find('.teacherGeart__text').slideUp();
		}
		else {
			$(this).addClass('open').find('.teacherGeart__text').slideDown();
		}
	});
	/*********************************************************************/
	$('.button-more').on('click', function() {
		$(this).parents('.splashfolioButton__in').parent().parent().find('.more_block').addClass('open');
	});
	/*********************************************************************/
	var headerBlock = 0;

	var line = $('#header_pager .headerLine_item');


	var slFoto = $('#studentsGallery__slider');

	function slFotos(visible){
		if (slFoto.length) {
			var prev      = slFoto.find('.prev'),
				next        = slFoto.find('.next'),
				sliderOurUl = slFoto.find('ul');

			sliderOurUl.carouFredSel({
				width: '100%',
				responsive: true,
				auto: false,
				prev: prev,
				next: next,
				items: {
					height : "auto",
					visible: visible
				},
				scroll: {
					items: visible
				},
				swipe: {
					onMouse: true,
					onTouch: true
				}
			});

			sliderOurUl.trigger('updateSizes');
		}
	}
	/*********************************************************************/
	var top_menu = $('.top_menu_block__menu');

	$('.top_menu_block__burger').on('click', function(){
		if (!$(this).hasClass('open')) {
			$(this).addClass('open');
			top_menu.fadeIn();
		}
		else {
			$(this).removeClass('open');
			top_menu.fadeOut();
		}
	});
	/*********************************************************************/
	$('.headerStr__lang, .headerStr__profile').on('click', function(){
		$('.top_menu_block__menu .open').removeClass('open').find('ul').fadeOut();

		if (!$(this).hasClass('open')) {
			$(this).addClass('open').find('ul').fadeIn();
		}
		else {
			$(this).removeClass('open').find('ul').fadeOut();
		}
	});
	/*********************************************************************/
	$(document).click(function(e){
		if (!$(e.target).closest(".top_menu_block__menu").length) {
			$('.headerStr__lang, .headerStr__profile').removeClass('open').find('ul').fadeOut();
		}
		e.stopPropagation();
	});
	/*********************************************************************/
	$.fancybox.defaults.touch = {
		vertical: false, // Allow to drag content vertically
		momentum: false // Continue movement after releasing mouse/touch when panning
	};

	$('.headerPlay, .play_trailer').on('click', function(){
		$.fancybox.open({
			src  : '#headerPlay-popup',
			type : 'inline',
			opts : {
				afterShow : function( instance, current ) {
					var headerPlaySl = $('#headerPlay__slider');

					headerPlaySl.css('opacity', 1);

					function headerPlay(visible) {
						var prev = headerPlaySl.find('.prev'),
							next = headerPlaySl.find('.next'),
							sliderOurUl = headerPlaySl.find('ul');

						sliderOurUl.carouFredSel({
							width: '100%',
							responsive: true,
							auto: false,
							prev: prev,
							next: next,
							items: {
								height: "auto",
								visible: visible
							},
							scroll: {
								items: visible
							},
							swipe: {
								onMouse: true,
								onTouch: true
							}
						});

						sliderOurUl.trigger('updateSizes');
					}

					$(window).on('resize init', function(){
						var widthPage = $(window).width();

						if (widthPage < 520) {
							oneRunSl3.destroy(function () {
								headerPlay(1);
							});
						}
						else if (widthPage < 750 && widthPage > 520){
							oneRunSl3.reload(function () {
								headerPlay(2);
							});
						}
						else if (widthPage < 1250 && widthPage > 750){
							oneRunSl3.destroy(function () {
								headerPlay(3);
							});
						}
						else {
							oneRunSl3.reload(function () {
								headerPlay(4);
							});
						}
					}).trigger('init');
				}
			}
		});
	});
	/*********************************************************************/
	$('.relatedProducts-open').on('click', function(){
		$.fancybox.open({
			src  : '#featuredProducts-popup',
			type : 'inline',
			opts : {
				afterShow : function( instance, current ) {
					console.info( 'done!' );
				}
			}
		});
	});
	/*********************************************************************/
	$('.button-giveaGift').on('click', function(){
		$.fancybox.open({
			src  : '#giveaGift-popup',
			type : 'inline',
			opts : {
				afterShow : function( instance, current ) {
					console.info( 'done!' );
				}
			}
		});
	});
	/*********************************************************************/
	$('#exploreTeachers .splashfolioButton, .button-featuredgiveaGift').on('click', function(){
		$.fancybox.open({
			src  : '#featuredgiveaGift-popup',
			type : 'inline',
			opts : {
				afterShow : function( instance, current ) {
					console.info( 'done!' );
				}
			}
		});
	});
	/*********************************************************************/
	$('.button-join-community').on('click', function(){
		$.fancybox.open({
			src  : '#joint-popup',
			type : 'inline',
			opts : {
				afterShow : function( instance, current ) {
					console.info( 'done!' );
				}
			}
		});
	});
	/*********************************************************************/
	function slidingBlock(block, head, div) {
		block.find(head).on('click', function(){
			var parent = $(this).parent();

			if (!parent.hasClass('open')) {
				block.find('.open').removeClass('open');
				parent.addClass('open');

				block.find(div).hide();
				parent.find(div).slideDown();
			}
			else {
				parent.find(div).slideUp(200, function () {
					parent.removeClass('open');
				});
			}
		});
	}

	slidingBlock($('.tripPlanning'), '.tripPlanning_head', '.tripPlanning_block_in');

	slidingBlock($('#teacherGeart'), '.teacherGeart__title', '.teacherGeart__itemBlock');
	/*********************************************************************/
	$(window).on('resize init', function(){
		if ($(window).width() < 1015) {
			headerBlock = $(window).width();

			oneRun.reload(function () {
				header_carousel.trigger('distroy');

				projectSlider($('.headerBlock'));
			});
		}
		else{
			headerBlock = $('#header .headerBlock').width();

			oneRun.destroy(function () {
				$('.top_menu_block__menu').attr('style', '');

				$('.top_menu_block__burger').removeClass('open');

				header_carousel.trigger('distroy');

				projectSlider($('.headerBlock'));
			});
		}

		line.width(headerBlock / line.length);


		var widthPage = $(window).width();

		if (widthPage < 500) {
			oneRunSl.reload(function () {
				slFotos(1);
			});
		}
		else if (widthPage < 800 && widthPage > 500){
			oneRunSl.destroy(function () {
				slFotos(2);
			});
		}
		else if (widthPage < 1000 && widthPage > 800){
			oneRunSl.reload(function () {
				slFotos(3);
			});
		}
		else if (widthPage < 1400 && widthPage > 1000){
			oneRunSl.destroy(function () {
				slFotos(4);
			});
		}
		else {
			oneRunSl.reload(function () {
				slFotos(5);
			});
		}


		if (widthPage < 500) {
			oneRunSl2.reload(function () {
				slFotosPro(1);
			});
		}
		else if (widthPage < 1015 && widthPage > 500){
			oneRunSl2.destroy(function () {
				slFotosPro(2);
			});
		}
		else {
			oneRunSl2.reload(function () {
				slFotosPro(3);
			});
		}

	}).trigger('init');
	/*********************************************************************/
});



$(window).on("load", function(){
	$('#wrapper').css('opacity', 1);
});


/***/ })
/******/ ]);