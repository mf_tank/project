<?php

namespace app\models;

use app\components\UploadFileBehavior;
use Yii;

/**
 * This is the model class for table "trailer".
 *
 * @property int $id
 * @property int $tutorial_id
 * @property string $url
 * @property string $file
 * @property int $created_at
 * @property int $updated_at
 * @property int $author_id
 * @property int $updater_id
 */
class Trailer extends \app\components\BaseARecord
{
    public function behaviors()
    {
        return array_merge_recursive(parent::behaviors(), [
            'uploadFileBehavior' => [
                'class' => UploadFileBehavior::className(),
                'attributes' => [
                    'file'
                ]
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trailer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tutorial_id', 'created_at', 'updated_at'], 'required'],
            [['tutorial_id', 'created_at', 'updated_at', 'author_id', 'updater_id'], 'integer'],
            [['url', 'file'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('site', 'ID'),
            'tutorial_id' => Yii::t('site', 'Tutorial ID'),
            'url' => Yii::t('site', 'Url'),
            'file' => Yii::t('site', 'File'),
            'created_at' => Yii::t('site', 'Created At'),
            'updated_at' => Yii::t('site', 'Updated At'),
            'author_id' => Yii::t('site', 'Author ID'),
            'updater_id' => Yii::t('site', 'Updater ID'),
        ];
    }
}
