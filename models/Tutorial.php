<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tutorial".
 *
 * @property int $id
 * @property string $title
 * @property int $image_id
 * @property string $content
 * @property int $created_at
 * @property int $updated_at
 * @property int $author_id
 * @property int $updater_id
 * @property int $teacher_id
 */
class Tutorial extends \app\components\BaseARecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tutorial';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image_id', 'teacher_id', 'created_at', 'updated_at', 'author_id', 'updater_id'], 'integer'],
            [['teacher_id'], 'required'],
            [['content'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('site', 'ID'),
            'title' => Yii::t('site', 'Title'),
            'image_id' => Yii::t('site', 'Image ID'),
            'content' => Yii::t('site', 'Content'),
            'created_at' => Yii::t('site', 'Created At'),
            'updated_at' => Yii::t('site', 'Updated At'),
            'author_id' => Yii::t('site', 'Author ID'),
            'updater_id' => Yii::t('site', 'Updater ID'),
            'teacher_id' => Yii::t('site', 'Teacher ID')
        ];
    }
}
