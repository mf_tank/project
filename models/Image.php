<?php

namespace app\models;

use Yii;
use app\components\UploadFileBehavior;

/**
 * This is the model class for table "image".
 *
 * @property int $id
 * @property string $img
 * @property int $is_approved
 * @property int $created_at
 * @property int $updated_at
 * @property int $author_id
 * @property int $updater_id
 */
class Image extends \app\components\BaseARecord
{
    public function behaviors()
    {
        return array_merge_recursive(parent::behaviors(), [
            'uploadFileBehavior' => [
                'class' => UploadFileBehavior::className(),
                'attributes' => [
                    'img'
                ]
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_approved', 'created_at', 'updated_at', 'author_id', 'updater_id'], 'integer'],

            [['img'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg', 'on' => 'create'],
            [['img'], 'file', 'extensions' => 'png, jpg, jpeg', 'on' => 'update'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('site', 'ID'),
            'img' => Yii::t('site', 'Img'),
            'is_approved' => Yii::t('site', 'Is Approved'),
            'created_at' => Yii::t('site', 'Created At'),
            'updated_at' => Yii::t('site', 'Updated At'),
            'author_id' => Yii::t('site', 'Author ID'),
            'updater_id' => Yii::t('site', 'Updater ID'),
        ];
    }
}
