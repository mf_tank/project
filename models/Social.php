<?php

namespace app\models;

use app\components\BaseARecord;
use app\components\UploadFileBehavior;
use Yii;

/**
 * This is the model class for table "social".
 *
 * @property int $id
 * @property string $link
 * @property string $img
 * @property int $is_active
 * @property int $created_at
 * @property int $updated_at
 * @property int $author_id
 * @property int $updater_id
 */
class Social extends BaseARecord
{

    public function behaviors()
    {
        return array_merge_recursive(parent::behaviors(), [
            'uploadFileBehavior' => [
                'class' => UploadFileBehavior::className(),
                'attributes' => [
                    'img'
                ]
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'social';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['link', 'sort'], 'required'],
            [['is_active', 'sort'], 'integer'],
            [['link'], 'string', 'max' => 255],

            [['img'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg', 'on' => 'create'],
            [['img'], 'file', 'extensions' => 'png, jpg, jpeg', 'on' => 'update'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link' => 'Ссылка',
            'img' => 'Иконка',
            'is_active' => 'Активность',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'author_id' => 'Author ID',
            'updater_id' => 'Updater ID',
            'sort' => 'Сортировка'
        ];
    }
}
