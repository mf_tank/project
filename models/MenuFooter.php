<?php

namespace app\models;

use app\components\BaseARecord;
use Yii;

/**
 * This is the model class for table "menu_footer".
 *
 * @property int $id
 * @property int $column
 * @property string $name
 * @property string $link
 * @property int $is_active
 * @property int $sort
 * @property int $lang_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $author_id
 * @property int $updater_id
 */
class MenuFooter extends BaseARecord
{

    /**
     * @var array
     */
    public static $COLUMN_TYPE = [
        1 => '1 колонка',
        2 => '2 колонка',
        3 => '3 колонка'
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu_footer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['column', 'name', 'link', 'lang_id'], 'required'],
            [['column', 'is_active', 'sort', 'lang_id'], 'integer'],
            [['name', 'link'], 'string', 'max' => 255],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['lang_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'column' => 'Колонка',
            'name' => 'Название',
            'link' => 'Ссылка',
            'is_active' => 'Активность',
            'sort' => 'Сортировка',
            'lang_id' => 'Язык',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'author_id' => 'Author ID',
            'updater_id' => 'Updater ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Lang::className(), ['id' => 'lang_id']);
    }
}
