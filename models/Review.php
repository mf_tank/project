<?php

namespace app\models;

use Yii;
use app\components\BaseARecord;

/**
 * This is the model class for table "review".
 *
 * @property int $id
 * @property int $image_id
 * @property string $name
 * @property string $profession
 * @property string $review_text
 * @property int $stars
 * @property int $position
 * @property int $is_published
 * @property int $email
 */
class Review extends BaseARecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'review';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image_id', 'stars', 'position', 'is_published'], 'integer'],
            [['name', 'profession', 'review_text', 'email'], 'required'],
            [['review_text', 'email', 'name', 'profession'], 'safe'],
            [['name', 'profession'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('site', 'ID'),
            'image_id' => Yii::t('site', 'Image ID'),
            'name' => Yii::t('site', 'Name'),
            'profession' => Yii::t('site', 'Profession'),
            'review_text' => Yii::t('site', 'Review Text'),
            'stars' => Yii::t('site', 'Stars'),
            'position' => Yii::t('site', 'Position'),
            'is_published' => Yii::t('site', 'Is Published'),
            'email' => Yii::t('site', 'E-mail')
        ];
    }
}
