<?php
/**
 * Created by PhpStorm.
 * User: IF
 * Date: 08.06.2019
 * Time: 18:16
 */

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;

class PhotograperOptions extends Model
{
    public $title;
    public $description;
    public $block_count;
    public $button_caption;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'block_count', 'button_caption'], 'required'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 50],
            [['button_caption'], 'string', 'max' => 20],
            [['block_count'], 'integer']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('admin', 'Block title'),
            'description' => Yii::t('admin', 'Block description'),
            'block_count' => Yii::t('admin', 'Block count'),
            'button_caption' => Yii::t('admin', 'Button caption')
        ];
    }
}