<?php
/**
 * Created by PhpStorm.
 * User: IF
 * Date: 05.06.2019
 * Time: 20:59
 */
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\promo\OurPhotographers */

$this->title = $model->getUserName();
$this->params['breadcrumbs'][] = ['label' => 'our photographers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="photographer-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('admin', 'Show all'), ['index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('admin', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('admin', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('admin','Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>



    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'image_id',
            [
                'attribute' => 'image_id',
                'label' => 'Image',
                'value'=> $model->image->img,
                'format' => ['image',['width'=>'180']],
            ],
            'trailer_id',
            'dark_text',
            'created_at',
            'updated_at',
            'author_id',
            'updater_id',
        ],
    ]) ?>

</div>
