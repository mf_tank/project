<?php
/**
 * Created by PhpStorm.
 * User: IF
 * Date: 02.06.2019
 * Time: 21:18
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;


$this->title = Yii::t('admin', 'Translate');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="translate-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'message',
                'label' => Yii::t('admin', 'Message'),
                'value' => 'sourceMessage.message'
            ],
            'language',
            'translation',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
