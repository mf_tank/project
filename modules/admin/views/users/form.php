<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Button;
use kartik\select2\Select2;
use app\models\User;

?>

<?php $form = ActiveForm::begin([
    'id'=>'form',
    'layout' => 'default'
]); ?>


<?php
$model->roles = $model->getRoles();
echo $form->field($model, 'roles')->widget(Select2::classname(), [
    'data' => User::$ROLES_ALL,
    'language' => 'ru',
    'options' => ['placeholder' => 'Выберите роль'],
    'pluginOptions' => [
        'multiple' => true,
        'allowClear' => true
    ],
]);
?>

<?php echo $form->field($model, 'username');?>

<?php
    if($model->isNewRecord) {

        $model->password = '';
        $model->password_repeat = '';

        echo $form->field($model, 'password')->passwordInput();
        echo $form->field($model, 'password_repeat')->passwordInput();
    }
?>

<?php echo Button::widget([
    'label' => $model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Обновить'),
    'options' => ['class' => 'btn btn-primary']
]);
?>
<?php ActiveForm::end();?>


