<?php

namespace app\modules\admin\components;


use app\components\BaseController;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\User;

/**
 * Class AdminController
 * @package backend\components
 */
class AdminController extends BaseController
{

    /**
     * Шаблон
     * @var string
     */
    public $layout = 'main';

    public $lang = 'ru';

    public $active = 0;


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'create',
                            'update',
                            'delete',
                            'view',
                            'options',
                            'search'
                        ],
                        'allow' => true,
                        'roles' => [
                            User::ROLE_ADMIN
                        ],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }



}
