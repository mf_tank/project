<?php

namespace app\components;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\web\HttpException;


/**
 * Главная модель
 * Class BaseARecord
 *
 * @property timestamp  created_at    Дата Создания
 * @property timestamp  updated_at    Дата Изменения
 */
abstract class BaseARecord extends ActiveRecord
{

    /**
     * Папка загрузки файлов
     */
    const PATH_URL = '/uploads/';

    /**
     * Константа количество элементов в гриде
     */
    const PAGE_SIZE = 50;



    /**
     * Инициализация модели
     */
    public function init()
    {
        if(method_exists($this, ActiveRecord::EVENT_BEFORE_INSERT)) {
            $this->on(ActiveRecord::EVENT_BEFORE_INSERT,[$this, ActiveRecord::EVENT_BEFORE_INSERT]);
        }
        if(method_exists($this, ActiveRecord::EVENT_BEFORE_UPDATE)) {
            $this->on(ActiveRecord::EVENT_BEFORE_UPDATE,[$this, ActiveRecord::EVENT_BEFORE_UPDATE]);
        }
        if(method_exists($this, ActiveRecord::EVENT_AFTER_UPDATE)) {
            $this->on(ActiveRecord::EVENT_AFTER_UPDATE,[$this, ActiveRecord::EVENT_AFTER_UPDATE]);
        }
        if(method_exists($this, ActiveRecord::EVENT_AFTER_INSERT)) {
            $this->on(ActiveRecord::EVENT_AFTER_INSERT,[$this, ActiveRecord::EVENT_AFTER_INSERT]);
        }
        parent::init();
    }

    /**
     * Функция возвращает имя таблицы бд
     * @return string
     */
    public static function tableName()
    {
        return '{{%'.self::getNameTable().'}}';
    }

    /**
     * Функция возвращает имя таблицы бд без шаблона
     * @return string
     */
    public static function getNameTableNoPattern()
    {
        return (string)Yii::$app->components['db']['tablePrefix'].self::getNameTable().'';
    }

    /**
     * Возврашает имя класса без пространства имен
     * @return string
     */
    public static function getNameTable()
    {
        return substr(
            strtolower(
                preg_replace('/([A-Z][a-z]+)/', '${1}_', self::getNameClass())
            ), 0, -1
        );
    }

    /**
     * Получить имя класс без пространства имен
     */
    public static function getNameClass()
    {
        $nameClassArray = explode('\\', get_called_class());
        return end($nameClassArray);
    }

    /**
     * Поведения
     * @return array
     */
    public function behaviors()
    {
        return array_merge_recursive(parent::behaviors(), [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'author_id',
                'updatedByAttribute' => 'updater_id',
            ],
        ]);
    }

    /**
     * Функция для валидации общих правил моделей
     * @return array
     */
    public function  rules()
    {
        return array_merge_recursive(parent::rules(), []);
    }

    /**
     * Имя общих имен аттрибутов моделей
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge_recursive(parent::attributeLabels(), [
            'id' => Yii::t('app', 'id'),
            'date_created' => Yii::t('app', 'Дата создания'),
            'date_changed' => Yii::t('app', 'Дата изменения'),
        ]);
    }

    /**
     * Возвращает обект модели по id
     * @param $id - id модели
     * @return mixed $model | CHttpException
     * @throws HttpException 400 Неправильный запрос
     */
    public static function getModel($id)
    {
        $model = self::findOne($id);
        if(!$model) {
            throw new HttpException(400, Yii::t('app', 'Неправильный запрос'));
        }
        return $model;
    }
}
