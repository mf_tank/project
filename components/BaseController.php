<?php

namespace app\components;

use app\models\Lang;
use app\models\MenuFooter;
use app\models\MenuHeader;
use Yii;

use yii\web\Controller;
use app\utils\LanguageUtil;

/**
 * Class BaseController
 * @package app\modules\admin\components
 */
abstract class BaseController extends Controller
{

    /**
     * Шаблон
     * @var string
     */
    public $layout = 'main';

    public $lang = 'ru';

    public $active = 0;

    public $menuHeader;

    public $language;

    public $languages;

    public $menuFooter1;

    public $menuFooter2;

    public $menuFooter3;


    /**
     * @param \yii\base\Action $event
     * @return bool
     */
    public function beforeAction($event)
    {
        \Yii::$app->language = (new LanguageUtil())->getCode();

        $this->menuHeader = MenuHeader::find()
            ->where(['is_active' => 1])
            ->orderBy('sort')
            ->all();

        $this->language = (new LanguageUtil())->getName();
        $this->languages = Lang::find()->all();

        $this->menuFooter1 = MenuFooter::find()
            ->where([
                'is_active' => 1,
                'column' => 1
            ])
            ->orderBy('sort')
            ->all();
        $this->menuFooter2 = MenuFooter::find()
            ->where([
                'is_active' => 1,
                'column' => 2
            ])
            ->orderBy('sort')
            ->all();
        $this->menuFooter3 = MenuFooter::find()
            ->where([
                'is_active' => 1,
                'column' => 3
            ])
            ->orderBy('sort')
            ->all();

        return parent::beforeAction($event);
    }
}
