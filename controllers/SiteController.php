<?php

namespace app\controllers;

use app\components\BaseController;
use app\models\Banner;
use app\models\Review;
use app\models\Feedback;
use app\models\MenuHeader;
use app\models\Options;
use app\models\Social;
use app\models\Teacher;
use app\models\promo\OurPhotographers;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Lang;
use app\utils\LanguageUtil;

class SiteController extends BaseController
{
    const DEFAULT_TEACHER_COUNT = 2;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $options_count = Options::findByKey('promo_our_photographers_count');
        $options_title = Options::findByKey('promo_our_photographers_title');
        $options_description = Options::findByKey('promo_our_photographers_description');
        $options_button = Options::findByKey('promo_our_photographers_button');

        $title = $options_title ? $options_title->value : '';
        $description = $options_description ? $options_description->value : '';
        $button = $options_button ? $options_button->value : '';

        $banners = Banner::find()
            ->where(['is_active' => 1])
            ->orderBy('sort')
            ->all();

        $socials = Social::find()
            ->where(['is_active' => 1])
            ->orderBy('sort')
            ->all();

        $our_photographers = OurPhotographers::find()
            ->limit($options_count ? $options_count->value : self::DEFAULT_TEACHER_COUNT)
            ->all();

        $reviews_count = Review::find()->count();
        $reviews = Review::find()->limit(3)->all();

        return $this->render('index', [
            'promo_our_photographers_title' => $title,
            'promo_our_photographers_description' => $description,
            'promo_our_photographers_button' => $button,
            'banners' => $banners,
            'socials' => $socials,
            'our_photographers' => $our_photographers,
            'reviews_count' => $reviews_count,
            'reviews' => $reviews
        ]);
    }

    /**
     * Set language id
     *
     * @param $id integer language id
     */
    public function actionLang($id)
    {
        (new LanguageUtil())->setId($id);

        $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * @return string
     */
    public function actionContacts()
    {
        return $this->render('contacts');
    }

}
