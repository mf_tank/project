<?php
/**
 * Created by PhpStorm.
 * User: IF
 * Date: 16.06.2019
 * Time: 23:05
 */

namespace app\controllers;

use Yii;
use app\models\Review;
use app\components\BaseController;

class ReviewsController extends BaseController
{
    public function actionIndex()
    {
        $reviews_count = Review::find()->count();
        $reviews = Review::find()->where(['is_published' => 1])->all();

        return $this->render('index', [
            'reviews_count' => $reviews_count,
            'reviews' => $reviews
        ]);
    }

    public function actionWrite()
    {
        $model = new Review();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->redirect(['reviews/index']);
        }

        return $this->render('write', [
            'model' => $model
        ]);
    }
}