<?php

namespace app\controllers;

use app\components\BaseController;
use app\models\Banner;
use app\models\MenuHeader;
use app\models\Social;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Lang;
use app\utils\LanguageUtil;
use app\models\Tutorial;

/**
 * Class TutorialsController
 * @package app\controllers
 */
class TutorialsController extends BaseController
{


    public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionView($id)
    {
        if (!$tutorial = Tutorial::findOne($id)) {
            $this->redirect('/notfound');
        }

        return $this->render('view', [
            'tutorial' => $tutorial
        ]);
    }
}
